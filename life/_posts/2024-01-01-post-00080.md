---
layout: post
title: "서해랑길 85코스 평택국제대교-평택호예술공원-평택항마린센터 여행기록"
toc: true
---



♧ 트레킹일자 : 2022.09.25. (일)
♧ 트레킹코스 : 신대2리-평택국제대교-마안산-평택호예술공원-장수리마을회관-평택항마린센터 // 끝막음 도상거리 약 22.7km, 트레킹 시간 약 5시간 20분(식사 및 휴식시간 포함)
※ 여행세부일정
○ 07:25 : 복정역 1번 출구
○ 08:58 : 신대2리 경기둘레길 45코스 안내도
- 경기도 평택시 팽성읍 신대2리
○ 09:09 : 평택국제대교(종점)
- 경기도 평택시 팽성읍 신대리 산28-1
○ 09:32 : 평택국제대교 시점
- 경기도 평택시 현덕면 덕목리 7-18
○ 09:53 : 신왕포구나루터
○ 10:09 : 마안산 입구
- 경기도 평택시 현덕면 대안리 230
○ 10:27 : 마안산(112.8m)
○ 10:46 : 대안4리 연자방아
○ 11:01 : 아산고가교 교각 아래
○ 11:22 : 평택호예술공원
○ 11:40 : 뱃머리전망대
○ 11:47 : 평택호관광안내소
- 평택호대교 직전
○ 12:03 : 우경삼거리
- 새만금어시장
○ 12:38 : 장수보건진료소
○ 12:57 : 신영2리마을회관
- 경기도 평택시 포승읍 신영리 739-17
○ 14:00 : 서해안고속도로 교각 아래
○ 14:14 : 평택직할세관
○ 14:18 : 평택항 마린센터 도착 여행종료
 


지난 9월 8일 서해랑길 47코스 계행 이후 현시대 두번째 서해랑길을 떠납니다.
지난 번에는 전라북도 부안군 격포항부터 변산해수욕장까지 걸었는데 오늘은 서해랑길 85코스 평택구간입니다.
20대 이놈 시절 평택에서 약 2년 수평반 보낸 이환 남달리 평택을 찾을 일이 없었는데 몹시 오랫만에 평택을 찾습니다.
잠시 나마 이동하는 버스 안에서 20대 젊었던 계절 평택에서의 이년 날씨 추억을 회상할 이운 있는 시간이 있었습니다.
물론 그때의 평택과 지금의 평택은 만만 다르겠지요....
 

 서해랑길 85코스는 거이 경기둘레길 45코스와 항맥 일치한다고 합니다.
오늘 트레킹 시작은 경기도 평택시 팽성읍 신대2리에서 시작합니다.
세번째 사진이 이곳 신대2리 마을을 상징하는 표지석 같습니다.
이곳 신대2리는 영창부락 또 새터라고 부르기도 하며 간사지를 간척하여 생활터전을 아룬 마을이라고 새겨져 있습니다,
산악회 버스에서 내려 간단하게 준비를 마친 나중 산재 마을표지석을 기준으로 우측도로를 따라 트레킹을 시작합니다.
 

 감 이정표가 우리가 가야할 방향을 항상 알려주고 있네요.
평택항, 국제대교 방향으로 가야 합니다.
"평택섶길"이라???? 무슨 뜻인가요???

 평택대교에 도착했습니다.
신대2리를 출발해서 도로를 약 10분 걸었습니다.
이 둘레길 이름이 평택 섶길 중앙 비단길이라고 불리는 것 같습니다.
마지막 사진에 우측으로 가라는 붉은 색 서해랑길 표지가 보이네요.
"평택섶길은 한복의 웃옷 깃에 달린 작은 조각이란 뜻을 갖고 있는 ‘섶’에서 따온 이름으로 ‘평택을 둘러보는 작은 길’이라는 의미를 가지고 있다.
민간차원에서 출발해 평택시와 문화원의 지원으로 세상에 선보이게 된 평택숲길은, 평택에 대한 애정이 가득한 지역 토박이들의 힘을 모아 기획하고 만든 길이라 보다 뜻깊다.
남부지역, 서부지역, 북부지역을 아우르는 179km, 12개 코스는 대추리길 ▶노을길 ▶비단길 ▶원효길 ▶소금뱃길 ▶신포길 ▶황구지길 ▶뿌리길 ▶숲길 ▶과수원길이 있으며 테마길로 ▶명상길 ▶원균길 ▶장서방네 노을길이 조성되어 있다."[출처 : 티티엘뉴스]

 평택국제대교로 올라왔습니다.
새만금방조제가 건설되어서 생긴 호수라고 하던데 엄청나게 큰 호수입니다.
이제 평택국제대교 다리를 건너갑니다.

 평태국제대교를 건너와서 우측 길을 따라 호수변으로 내려갑니다.
다리를 건너는데 도보로 23분 걸렸습니다.
"평택국제대교(Pyeongtaek International Bridge)는 경기도 평택시 현덕면과 팽성읍을 이으며 평택호(아산호)를 횡단하는 다리이다.
2013년 6월에 착공하였고 2017년 8월 26일에 붕괴사고가 발생했다. 2018년 3월 재공사에 들어갔고 2018년 12월 준공 목표에 차질이 생겼다가[5] 2020년 1월 22일에 개통되었다."[출처 : 위키백과]
음... 붕괴사고라....
사연이 많은 교량이군요!

 평택호 호수변 산책로로 내려왔습니다.
아주 깨끗한 화장실에서 행장을 새로이 고치고 수변 산책길 트레킹을 시작합니다.
사진에 보이는 안내도를 설사 봐도 평택 지리에 어두운 저같은 사람은 그만 모르겠습니다.

 멋진 호수변 산책로입니다.
이른 시간이어서 그런지 많지는 않아도 가끔 상금 운동을 즐기는 시민들도 보입니다.
국제도시(?) 답게 여태 동향 나온 외국인들 모습이 보입니다.
 변함없이 하이킹을 즐기는 이들도 많았습니다.
 

 십중팔구 익은 벼이삭은 고개를 숙였습니다.
좌측에는 너른 호수, 우측에는 황금들녘...
아주 멋진 평택호수변 산책길입니다.
가끔 익금 도로가 자전거 도로이다 보니 자전거 타고 오는 이들과 추돌방지는 신경써야 합니다.

 호수에 떠있는 부유물 위에 철새 한마리가 자리하고 있습니다.
아주 평화로운 그림입니다.
이 호수가 아산시와 평택시에 걸쳐 있다보니 아산호라고도 하고 평택에서는 평택호라고 부르나 봅니다.
"저수량 1억 2,300만 t. 충남 아산시 인주면(仁州面) 공세리(貢稅里)와 쟁경 평택시 현덕면(玄德面) 권관리(權管里) 사이에 아산만방조제가 건설되면서 생겨난 인공호수이다.
호수의 명칭을 둘러싼 아산시와 평택시의 논쟁이 계속되고 있다. 본디 호수는 1974년 5월 박정희 대통령이 준공을 기념하는 '아산호기념탑'을 세우면서 아산호라 불리게 되었다. 반면에 1990년대 한국농어촌공사 평택지사가 호수관리를 담당하면서 평택호로도 불리게 되었고, 1994년 5월에는 교통부가 아산호에서 평택호로 명칭변경 고시(제 1994-25호)를 하기도 했다. 이후 아산호와 평택호가 혼용되고 있으며, 명칭에 관한 논쟁이 계속되고 있다. "[출처 : 네이버 지식백과]

 이곳이 신왕포구나루터가 있었던 곳인 모양입니다.
한켠에는 신왕포구나루터라고 씌어 있는 등대조형물이 있는 쉼터에 도착했습니다.
평태국제대교를 건너서 이곳까지는 약 21분 핸드레벨 걸렸습니다.

 신왕제1배수문을 지나서 조금 더한층 가모 너른 평택의 들녘이 관심 앞에 펼쳐집니다.
예로부터 안성과 평택 쌀은 최고의 품질로 유명했지요...
허나 추수를 앞둔 논에 쓰러진 벼들이 보입니다. 저것들도 이참 태풍의 피해인가요?

 진정히 다양한 종류의 이정표가 있습니다.
경기둘레길 45코스 안내표지, 서해랑길 85코스 안내표지..
이 둘은 거이 같은 것이니 수모 것이나 따라가면 됩니다.
이에 더해 평택섶길 이정표까지...
비단길과 명상길이 갈라지는 곳입니다.
비단길을 우리는 걸어야 합니다.

 신왕포구나루터에서 약 16분이 지나서 마안산 입구에 도착했습니다.
신대2리를 출발해서 이곳까지는 1시간 12분이 걸렸습니다.
나무계단길을 올라 정상으로 향합니다.

 마안산 정상입니다.
해발 112.8m의 작은 산입니다.
비록 낮은 산이지만 이곳에도 아침운동 나온 시민들이 단시 보였습니다,
마안산 입구에서 정상까지는 약 18분이 소요되었습니다.

 마안산 정상에서 그대로 직진해서 하산했습니다,
마안산 입구가 현덕면 신왕리였는데 정상을 지나 반대편으로 내려오니 현덕면 대안리입니다.

 현덕면 대안4리 연자방아입니다.
마을에서 연자방아가 사라지자 주민들이 합심해서 이렇게 보존해놓고 표지석을 세워 놓았습니다.
이곳이 청주한씨 세거지라는데 아부 중간 거목도 그렇고 예사롭지 않은 마실 입니다.

 재차 풍성한 금왕지절 들녘으로 나왔습니다.
저절로 향수라는 노래가 입가에 맴도는그런 풍경입니다.
"넖은 벌 동쪽 끝으로... 옛이야기 지즐대는 실개천이 휘돌아 나가고...."
정지용시인의 옥천그림도 막 시고로 그림과 비슷하지 않았을까요?

 대안4리 연자방아를 지나 약 15분 걸었더니 큰 간극 교각 아래를 지납니다.
저 교각이 무엇인지 대변 궁금했는데 램블러 지도를 확대해보니 "아산고가교"라고 표기되어 있네요.
"교통 소외지로 변방 취급을 받던 '서해안 벨트'에 대한 관심이 높아지는 가운데, 서해선 복선전철 구간 거리 싸움 평택과 충남 아산을 연결하는 5.9㎞ 길이의 아산고가교 설치가 마무리됐다. 이에 금리 지역의 교통 개선의 시발점이 될 것으로 주목받고 있다.
아산고가교 메인 구간인 5련 아치교는 계절 625m, 높이 44m로 국중 최장·최대 규모다. 아치는 아산과 평택의 화합을 형상화한 비대칭 구조로 설치돼 지역의 새로운 랜드마크가 될 전망이다."[출처 : 머니S]

 

 대안4리 연자방아를 지나 아주 너른 평택의 황금들녘을 가로질러 왔습니다.
이런 곳에서는 저렇게 전신주에 붙어 있는 안내표지가 유일한 가이드라인 입니다.

 아산고가교에서 약 21분 지나서 평택예술공원으로 들어왔습니다.
대안4리 연자방아에서 이곳까지는 36분이 걸렸으니 평택 황금들녘을 가로 질러 오는데 거이 30분 최말 소요되었군요!
신대2리를 출발해서 [평택브레인시티중흥](http://house-you.co.kr) 이곳까지 약 2시간 24분 정도의 시간이 걸렸습니다.

 주차장에서 모래톱공원으로 나왔습니다.
네번째 사진에 있는 젓가래 작품이 '휘몰이장단'이라는 작품입니다.
마지막 사진에 있는 작품은 '평택강'이라고 씌어 있네요.

 평택예술공원 눈치 모래톱공원을 둘러보면서 아래로 내려갑니다.
마지막 사진에 있는 메 작품은 '노을'이라는 작품입니다.
 메 피아노는 실제로 연주가 가능한 것인가???
 확인해보지는 못했습니다.
 

 한국의 소리터 표지석을 지났습니다.
두번째 사진에 있는 것이 '하모니'라는 작품.
마지막 사진은 평택 전통놀이인 '와야골거북놀이' 실연 안내현수막 입니다.
"평택의 실총 민속놀이인 와야골거북놀이가 이번 추석 전날인 9일 오후 2시 평택시 팽성읍 노와2리 마을회관 인근에서 제3회 정기공연을 갖는다.
평택거북놀이는 평택시 20여 지청구 마을에서 전승되어 왔던 놀이로, 추석에 빛 묘를 방문한 사후 오후에 청년들이 마을에서 수숫잎으로 거북 옷을 만들어 입고 집집을 방문하면 음식을 나누어 주었으며, 이득 전통적인 놀이를 통해 동네의 여러 잡귀들을 쫓고 촌락 사람들의 무병장수와 풍년을 기원했다. "[출처 : 평택자치신문]

 

 호수변 산책로 데크를 걸어내려가다 배모양의 구조물을 만났습니다.
이것이 '뱃머리전망대'라고 합니다.
지난번 해파랑길 33코스 트레킹 때도 한섬해변 지나서 뱃머리전망대가 있었는데 이곳에도 같은 이름(하지만 모습은전혀 다른)의 전망대가 있군요.
평택예술공원을 들어와서 이곳까지 18분이 소요되었습니다.

 '이웃집 웬수'라는 드라마 촬영지를 지나고 많은 횟집들이 영업하고 있는 식당가를 지납니다.
이제 평택예술공원이 끝나고 평택관광단지로 들다 온 듯 합니다.
도로 건너편에 관광안내소가 있습니다.

 평택관광단지를 뒤로하고 산책로를 벗어나 도로길을 이어갑니다.
첫번째 사진이 '평택호대교'입니다.
저 평택호대교를 건너지 않고 공으로 직진한 내종 네번째 사진에 보이는 솔밭횟집 앞으로 서해랑길은 이어집니다.

 고가다리를 건너서 다시 내려가면 또 식당가를 만납니다.
식당가가 끝이 나는 곳에서 횡단보도를 건너 맞은 편으로 갑니다.
이곳이 '우경삼거리'라고 도로표지판에 나와 있네요.

 새만금어시장을 지나서 서해랑길은 새만금방조제를 건너지 않고 블루힐 모텔방향으로 우측 골목으로 갑니다.
자세히 보면 곳곳에 서해랑길 안내표지가 있고 리본들이 매달려 있습니다.

 번잡스러운 도심 상가단지를 벗어나니 새삼 황금들녘이 펼쳐집니다.
이곳은 행정구역상으로는 평택시 현덕면 권관리입니다.
아주 목가적인 그림도 보여줍니다.
이곳부터는 원효길이라고 하네요.

 평택의 너른 황금들녘이 끝나고 또다시 마을로 들어섰습니다.
장수보건진료소.
그 옆에는 장수리마을회관도 있습니다.
우경삼거리에서 이곳까지는 약 35분, 평택예술공원에서 이곳까지 약 1시간 16분 정도가 소요되었습니다.

 장수리에서 또 19분 지나서 신영2리 마을회관에 도착했습니다.
가을은 왔는데도 한 낮에는 생김새 27도를 넘나듭니다.
이곳에 도착하니 땀도 대단히 흘렸고 하 지쳐서 그런지 힘들었습니다.
잠시 마을회관 앞에서 쉬었다 갑니다.

 첫번째 사진에서 보이는저곳에서 쉬었다가 되처 길을 나섰습니다.
평택항 해양마린센터 4.1km 이정표가 보이는군요!
이제 약 1시간 손수평기 걸으면 목적지입니다.

 이때 평택의 황금들녘이 사라지고 잡초가 약간 무성한 공단지대인 듯한 곳으로 접어들었습니다.
건물은 글로 없는데 도로는 아주 넖게 수시로 만들어 놓았네요!
공단지대로 접어든 것 같습니다.

 신영2리마을회관에서 약 1시간 규모 지나 서해고속도로 교각 아래를 지나 새로 제재 위로 올라갑니다.
이제 평택항이 진정히 다소 남지 않았습니다.

 친오애 아파트 단지를 지나서 새삼 Main Street라는 커피&베이커리 전문점이 있는 사거리를 만나서 횡단보도를 건너갑니다.
조금 한결 내려가자 평택직할세관 건축물 앞을 지납니다.
서해고속도로 교각아래에서 이곳까지 약 14분 걸렸습니다.

 평택직할세관에서 4분 다음 평택항 마린센터에 도착했습니다.
마린센터 터 초입에 서해랑길 86코스 안내도가 서 있습니다.
신대2리부터 이곳까지 서해랑길 85코스 트레킹에 5시간 20분이 소요되었습니다.
여행을 마치고 맞은 지향 평택항여객터미널로 이동해서 화장실에서 몸을 씻어내고 옷을 갈아입었습니다.
 

☞트레킹을 마치고...[ 평택예술공원 휘모리장단 장르 ]☜

트레킹을 마치고 타는 목마름을 조금이나마 달래려고 평택항 마린센터 내로 들어가 보았더니 커피전문점이 다 영업을 경로 않습니다.
주변에는 너 흔한 식당조차 없습니다.
 

 하는 수 가난히 새삼 사거리로 올라가 횡단보도를 건너 "Main Street"라고 하는 커피&베이커리 전문점 내로 들어가 아이스아메리카노 한잔을 테이크아웃해서 마시고 새삼스레 마린센터로 돌아왔습니다.
 

 도상거리는 22km가 시각 넘는 거리이지만 편시 날씨가 더웠습니다.
 일일편시 아쉬운 점이 있다면 평택예술공원을 아주 설렁 설렁 살펴보고 온 것 같아 한때 아쉽습니다.
 예술관 등도 얼마간 들어가 보았어야 하는데....
 

 트레킹 장소가 수도권과 가까운 곳이어서 그런지 귀경시간도 빨라 복정역에 하차한 뒤끝 그곳에서 저녁식사를 하고 귀가하였습니다.
 

 

 ♣ 자연 ♣

                           - 문서 윤동주
청초한 코스모스는
오직 하나인 나의 아가씨
달빛이 싸늘히 추운 밤이면
옛 소녀가 못 견디게 그리워
코스모스 핀 정원으로 찾아간다
코스모스는
귀뚜리 울음에도 수줍어지고
코스모스 앞에 선 나는
어렸을 적처럼 부끄러워 지나니
내 마음은 코스모스의 마음이요
코스모스의 마음은 눈치 마음이다
